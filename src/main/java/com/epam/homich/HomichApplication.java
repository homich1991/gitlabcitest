package com.epam.homich;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomichApplication {

    public static void main(String[] args) {
        SpringApplication.run(HomichApplication.class, args);
    }

}
